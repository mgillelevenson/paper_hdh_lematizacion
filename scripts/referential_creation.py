import re


# Ce script permet à partir du référentiel de Sánchez Marco de créer les listes de contrôles pour la correction avec Pyrrha.

def creation_list(filesname):
    maliste = []
    # on transforme les référentiels en liste de listes, chaque sous-liste étant organisée comme telle: [forme, lemme, pos]
    for fichier_dict in filesname:
        fichier = open(fichier_dict, 'r')
        for line in fichier.readlines():
            resultat = re.split(r'\s+', line)
            maliste.append(resultat[1])
    maliste = list(set(maliste))
    print(len(maliste))
    return maliste





def main():
    maliste_oldesp = creation_list(["/usr/local/share/freeling/es/es-old/old-es.adj",
                                    "/usr/local/share/freeling/es/es-old/dicc.src",
                              "/usr/local/share/freeling/es/es-old/old-es.nom",
                              "/usr/local/share/freeling/es/es-old/old-es.int",
                              "/usr/local/share/freeling/es/es-old/old-es.tanc",
                              "/usr/local/share/freeling/es/es-old/old-es.nom",
                              "/usr/local/share/freeling/es/es-old/old-es.vaux",
                              "/usr/local/share/freeling/es/es-old/old-es.verb",
                              "/usr/local/share/freeling/es/es-old/old-es.num",
                                    "/usr/local/share/freeling/es/es-old/old-es.tanc"])
    
    malist_esp = creation_list(["/usr/local/share/freeling/es/dicc.src"])

    other_lemmas = creation_list(["/usr/local/share/freeling/es/treeler/maps/lemmas.map"])

    merged_list = list(set(malist_esp + maliste_oldesp + other_lemmas))
    merged_list.sort()

    with open('referencial/referentiel.txt', 'w') as f:
        for entree in merged_list:
            f.write(f"{entree}\n")




if __name__ == '__main__':
    main()
