def txt_to_list_of_lemmas(file: str):
    '''
    This function takes a text file and converts it into
    a list of lists given some separator (default a space).
    :param file:
    :return:
    '''
    with open(file, 'r') as text_file:
        lines = [line.rstrip('\n') for line in text_file]
        list_to_evaluate = [splitted_line.split(f" ")[1] for splitted_line in lines if len(splitted_line) > 1 and not splitted_line.split(" ")[2].startswith("NP") and not splitted_line.split(" ")[2].startswith("RG")]
        return list_to_evaluate[:-1]

def txt_to_list(file: str):
    '''
    This function takes a text file and converts it into
    a list of lists given some separator (default a space).
    :param file:
    :return:
    '''
    with open(file, 'r') as text_file:
        lines = [line.rstrip('\n') for line in text_file]
        list_to_evaluate = [splitted_line.split(f" ") for splitted_line in lines]
        return list_to_evaluate

def control_list_to_list(file: str):
    '''
    This function takes a text file and converts it into
    a list of lists given some separator (default a space).
    :param file:
    :return:
    '''
    with open(file, 'r') as text_file:
        lines = [line.rstrip('\n') for line in text_file]
        return lines

def target_list_to_correct(liste_a_verifier, corpus_corrige):
    output_list = []
    for analysis in corpus_corrige:
        intermed_list = []
        if len(analysis) > 2:
            form, lemma, *_ = analysis
            if lemma in liste_a_verifier and lemma not in [',', '.', ':', ';', '?'] and not '+' in lemma and not 'NP000P0' in analysis:
                out = [form, lemma, *_, "VERIF"]
                intermed_list.extend(out)
            else:
                intermed_list.extend(analysis)
        else:
            intermed_list.append(analysis)
        output_list.append(intermed_list)
    print(output_list)
    output_list = [" ".join(intermed) if (len(intermed) > 1) else "    " for intermed in output_list]

    with open("results/corpus_corrected.txt", "w") as output_file:
        output_file.write("\n".join(output_list))



def main():
    liste_controle = control_list_to_list("referentiel.txt")
    liste_lemmes = txt_to_list_of_lemmas("corpus/corpus_hdh/acentos/target/corpus.txt")


    nouveaux_lemmes = [lemme for lemme in liste_lemmes if lemme not in liste_controle]
    nouveaux_lemmes = list(set(nouveaux_lemmes))
    nouveaux_lemmes.sort()
    with open("results/lemmes_a_verifier.txt", "w") as output_file:
        output_file.write("\n".join(nouveaux_lemmes))

    corpus_as_list = txt_to_list("corpus/corpus_hdh/acentos/target/corpus.txt")

    corpus_maj = target_list_to_correct(nouveaux_lemmes, corpus_as_list)
if __name__ == '__main__':
    main()