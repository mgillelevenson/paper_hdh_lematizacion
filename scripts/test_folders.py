import glob
import re
import os



def main():
    with open("/home/mgl/Dropbox/HDH/corpus_lematizado_corregido.tsv", "r") as input_corpus:
        liste_analyses = [ligne.replace("\n", "").replace("\t", " ") for ligne in input_corpus.readlines()[:-1]]
    regexp = r'\[.*\].*'
    liste_index = []
    for index, analyse in enumerate(liste_analyses):
        if re.match(regexp, analyse):
            liste_index.append(index)

    output_list = []

    for index, value in enumerate(liste_index[1:]):
        output_list.append((liste_index[index], value))
    # gestion du dernier élément de la liste
    output_list.append((liste_index[-1], len(liste_analyses)))

    liste_identifiants = []
    for borne_inf, borne_sup in output_list:
        # On commence par distribuer les textes dans leur dossier respectif.
        texto_corregido = liste_analyses[borne_inf:borne_sup]
        identifiant = texto_corregido[0].split(" ")[0].replace("[", "").replace("]", "")
        liste_identifiants.append(f"{identifiant}")
        with open(f"/home/mgl/Dropbox/HDH/corpus/{identifiant}/texto_lematizado_corregido_auto/texto.txt", "w") as text_to_print:
            target_string = "\n".join(texto_corregido)
            text_to_print.write(target_string)


        with open(f"/home/mgl/Dropbox/HDH/corpus/{identifiant}/texto_corregido/texto.txt", "w") as text_to_print:
            liste_de_formes = [x.split(" ")[0] for x in texto_corregido]
            print(liste_de_formes)
            text_to_print.write("\n".join(liste_de_formes))

    check_files(liste_identifiants)


def check_files(list_to_check):
    # https://stackoverflow.com/a/59938961
    liste_d_oeuvres = [f.path.split("/")[-1] for f in os.scandir("/home/mgl/Dropbox/HDH/corpus/") if f.is_dir()]
    Diff(liste_d_oeuvres, list_to_check)

def Diff(li1, li2):
    li_dif = [i for i in li1 + li2 if i not in li1 or i not in li2]
    print(li_dif)

if __name__ == '__main__':
    # This script tests the integrity of the corpus.
    main()