import sys

with open(sys.argv[1], "r") as input_file:
    text_string = input_file.read().split("\n")

output_list = [ligne.split(" ")[0] for ligne in text_string]
accent_mapping = {"á": "a",
                  "é": "e",
                  "í": "i",
                  "ó": "o",
                  "ú": "u",
                  "ý": "y",
                  "ï": "i",
                  "Á": "A",
                  "É": "E",
                  "Í": "I",
                  "Ó": "O"}

output_list_no_accent = [ligne.split(" ")[0] for ligne in text_string]
for key, value in accent_mapping.items():
    output_list_no_accent = [item.replace(key, value) for item in output_list_no_accent]

with open("corpus/corpus_hdh/acentos/eval/formes_finales_accent.tsv", "w") as output_file:
    output_file.write("\n".join(output_list))


with open("corpus/corpus_hdh/sin_acento/eval/formes_finales_sans_accent.tsv", "w") as output_file:
    output_file.write("\n".join(output_list_no_accent))