# This script allows to evaluate the result of a lemmatisation against ground truths
import argparse
import os
import re


class Corpus:
    def __init__(self, path_to_corpus, ignore_ne_lemma, ignore_ne_pos, ignore_ne_morph, ignore_verb_type=True):
        # File management
        # Please give the corpus to evaluate
        self.path_to_corpus = path_to_corpus
        corpus_name = self.path_to_corpus.split('/')[-1]
        self.path_to_results =  f"results/{corpus_name}"
        self.eval_file = f"{path_to_corpus}/eval/corpus.txt"
        self.target_file = f"{path_to_corpus}/target/corpus.txt"
        try:
            os.mkdir(self.path_to_results)
        except:
            pass
        # Lists
        self.list_to_evaluate = []
        self.ground_truth_list = []
        self.lemma_vocab = []
        self.forms_vocab = []
        self.lemma_errors_stats_list = []

        # This is the important variable
        self.reference_aligned_list = []
        # This is the important variable


        self.ambiguous_tokens = []

        # Dictionnaries
        self.incorrect_forms = {}
        self.incorrect_pos = {}
        self.incorrect_lemma = {}
        self.incorrect_morph = {}
        self.lemma_confusion_dict = {}
        self.pos_confusion_dict = {}
        self.morph_confusion_dict = {}

        # Corpus metrics
        self.corpus_length = 0
        self.number_of_named_entities = 0
        self.lemma_vocab_length = 0
        self.forms_vocab_length = 0

        # Metric on errors
        self.global_errors = 0
        self.lemma_errors = 0
        self.pos_errors = 0
        self.morph_errors = 0
        self.separator = " "

        self.ratio_pos = 0
        self.ratio_lemma = 0
        self.ratio_morph = 0

        # The dict that contains
        self.target_dict = {}
        self.eval_dict = {}

        # Whether to take into account NE in the evaluation
        self.ignore_ne_lemma = ignore_ne_lemma
        self.ignore_ne_pos = ignore_ne_pos
        self.ignore_ne_morph = ignore_ne_morph
        self.ignore_verb_type = ignore_verb_type


        self.eval_no_split = self.txt_to_list(self.eval_file)
        self.target_no_split = self.txt_to_list(self.target_file)

        self.list_to_evaluate = []
        self.target_list = []

    def get_ambiguous_tokens_stats(self):
        """
        This function aims to get the statistics on the ambiguous tokens.
        :return:
        """
        targets = [tuple(target) for target, prediction in self.reference_aligned_list]
        print(len(targets))
        print(targets[:10])
        set_of_targets = list(set(targets))
        homographs_dict = {}
        for analysis in set_of_targets:
            if len(analysis) == 4:
                form, lemma, pos, morph = analysis
                try:
                    homographs_dict[form].append((form, lemma, pos, morph))
                except:
                    homographs_dict[form] = [(form, lemma, pos, morph)]

        homograph_list = []
        for key, value in homographs_dict.items():
            if len(value) > 1:
                homograph_list.append((key, value))

        readable = str()
        readable += "Token\tanalysis\n"
        for token, *analysis in homograph_list:
            readable += f"{token}\t{analysis}\n"

        with open(f"{self.path_to_results}/ambiguous_tokens.tsv", "w") as ambiguous_tokens_file:
            ambiguous_tokens_file.write(readable)


    def request(self, request:str, analysis:str, caption):
        """
        We get all predictions, filter by the request and search
        for predictions that do not match the corresponding target.
        This will get the error number for the given class (PoS or morph).
        We then divide this number by the total number of element of the class
        to get the error rate on this class.
        :param request: the request in form of a regular expression
        :param analysis: lemma, pos or morph ?
        :return:
        """
        morph_request = re.compile(request)
        error_number = 0
        number_of_specific_analysis = 0
        if analysis == "lemma":
            analysis_code = 1
        elif analysis == "pos":
            analysis_code = 2
        elif analysis == "morph":
            analysis_code = 3

        for target, prediction in self.reference_aligned_list:
            if len(target) == 4:
                if re.match(morph_request, target[analysis_code]):
                    number_of_specific_analysis += 1
                    if target[analysis_code] != prediction[analysis_code]:
                        error_number += 1
            else:
                print(target)
        print(f"Error rate for request {request} ({caption}):")
        percent = round((error_number / number_of_specific_analysis) * 100, 2)
        print(f"{percent}% ({error_number} / {number_of_specific_analysis})")

    def split_pos_morph(self):
        """
        This function splits the eagles tags to separe part of speech
        and morph. It also manages the contractions.
        :param input_list: list of list of the form:
        ['[accorsi_EstudioEspejoVerdadera_2011]', '[accorsi_EstudioEspejoVerdadera_2011]', 'Z', '1']
        :return: a list
        TODO: ignorer Auxiliaire / principal / semi aux pour les verbes
        """

        self.split(self.eval_no_split, self.list_to_evaluate, ignore_verb_type=self.ignore_verb_type)
        self.split(self.target_no_split, self.target_list, ignore_verb_type=self.ignore_verb_type)



    def split(self, orig_list:list, target_list:list, ignore_verb_type:bool):
        for line in orig_list:
            if "[" in line[0] and "]" in line[0]:
                target_list.append(line)
            elif len(line) < 3:
                target_list.append(line)
            else:
                # When there is no contraction: take the first two chars and you'll get your PoS, etc.
                if not "+" in line[2]:
                    target = [line[0], line[1], line[2][0:2], line[2][2:]]
                else:
                    # If it's a 2 words contraction, we have to distribute the PoS and the Morph.
                    if line[2].count("+") == 1:
                        target = [line[0], line[1],
                                  f"{line[2].split('+')[0][0:2]}+{line[2].split('+')[1][0:2]}",
                                  f"{line[2].split('+')[0][2:]}+{line[2].split('+')[1][2:]}"]
                    # Idem if it's a 2 words contraction, we have to distribute the PoS and the Morph.
                    elif line[2].count("+") == 2:
                        target = [line[0], line[1],
                                  f"{line[2].split('+')[0][0:2]}+{line[2].split('+')[1][0:2]}+{line[2].split('+')[2][0:2]}",
                                  f"{line[2].split('+')[0][2:]}+{line[2].split('+')[1][2:]}+{line[2].split('+')[2][2:]}"]
                # We just remove V, A, S in the new PoS tag.
                if ignore_verb_type:
                    target = [target[0], target[1], target[2].replace("VM", "V").replace("VA", "V").replace("VS", "V"), target[3]]
                target_list.append(target)

    def txt_to_list(self, file:str):
        '''
        This function takes a text file and converts it into
        a list of lists given some separator (default a space).
        :param file:
        :return:
        '''
        with open(file, 'r') as text_file:
            lines = [line.rstrip('\n') for line in text_file]
            list_to_evaluate = [splitted_line.split(f"{self.separator}") for splitted_line in lines]
            return list_to_evaluate[:-1]


    def global_result(self):
        """
        This function produces a document with the global statistics
        :return:
        """

        if self.ignore_ne_lemma:
            lemma_corpus_length = self.corpus_length - self.number_of_named_entities
        else:
            lemma_corpus_length = self.corpus_length


        if self.ignore_ne_pos:
            pos_corpus_length = self.corpus_length - self.number_of_named_entities
        else:
            pos_corpus_length = self.corpus_length


        if self.ignore_ne_morph:
            morph_corpus_length = self.corpus_length - self.number_of_named_entities
        else:
            morph_corpus_length = self.corpus_length

        self.ratio_lemma = f"{round((1 - (self.lemma_errors / lemma_corpus_length)) * 100, 2)} %"
        print(self.lemma_errors)
        self.ratio_pos = f"{round((1 - (self.pos_errors / pos_corpus_length)) * 100, 2)} %"
        self.ratio_morph = f"{round((1 - (self.morph_errors / morph_corpus_length)) * 100, 2)} %"
        print(self.morph_errors)


        with open(f"{self.path_to_results}/global_metrics.tsv", "a") as global_metrics:
            global_metrics.write(f"\n\nNE exclusion in lemma: {self.ignore_ne_lemma} | pos: {self.ignore_ne_pos} | morph: {self.ignore_ne_morph}\n"
                                 f"Number of tokens:\t{self.corpus_length}\n"
                                 f"Number of different lemmas:\t{self.lemma_vocab_length}\n"
                                 f"Number of different forms:\t{self.forms_vocab_length}\n"
                                 f"Error ratio on lemmas:\t{self.ratio_lemma} ({self.lemma_errors} / {lemma_corpus_length})\n"
                                 f"Error ratio on pos:\t{self.ratio_pos} ({self.pos_errors} / {pos_corpus_length})\n"
                                 f"Error ratio on morph:\t{self.ratio_morph} ({self.morph_errors} / {morph_corpus_length})")

        with open("referencial/list_of_lemmas.txt", "w") as list_of_lemmas:
            list_to_write = list(set(self.lemma_vocab))
            list_to_write.sort()
            list_of_lemmas.write("\n".join(list_to_write))



    def to_dict_of_texts(self, list_to_transform:list):
        '''
        This function transforms a list of texts with the id as the first value of a text
        into a dictionnary which key is the id.
        :param list_to_transform: the input list
        :return:
        '''
        output_dict = {}
        regexp = r'\[.*\]'
        for analysis in list_to_transform:
            if re.match(regexp, analysis[0]):
                current_text = analysis[0]
                output_dict[analysis[0]] = []
            else:
                output_dict[current_text].append(analysis)
        return output_dict

    def print_forms_list(self, dictionnary):
        output_dictionnary = {}
        for correct_analysis, value in dictionnary.items():
            intermed_dict = {}
            for tuple in value:
                try:
                    intermed_dict[tuple] += 1
                except:
                    intermed_dict[tuple] = 1
            occurrences = sum([occ for occ in intermed_dict.values()])
            intermed_list = [[key, value] for key, value in intermed_dict.items()]
            intermed_list.append(occurrences)
            output_dictionnary[correct_analysis] = intermed_list
        output_list = [[key, value] for key, value in output_dictionnary.items()]
        output_list.sort(reverse=False, key=lambda x: x[0])

        readable = str()
        readable += f"form predicted_lemma target_lemma predicted_pos predicted_morph occurrences\n"
        for form, list_of_analysis in output_list:
            # total_occurrence = list_of_analysis[-1]
            list_of_analysis = list_of_analysis[:-1]
            for analysis, occurrence in list_of_analysis:
                to_string = [str(element) for element in analysis]
                readable += f"{form} {' '.join(to_string)} {str(occurrence)}\n"

            with open(f"{self.path_to_results}/forms_quali_wrt_lemmas.txt", "w") as quali:
                quali.write(readable)

    def print_confusion_dict(self, dictionnary:dict, title):
        """
        This function takes a dictionnary {Correct_analysis: [(A), (B), (A)]} and
        Returns {Correct_analysis: [(A: 2), (B: 1)]}
        :param dictionnary:
        :return: a dict
        """
        output_dictionnary = {}
        for correct_analysis, value in dictionnary.items():
            intermed_dict = {}
            for tuple in value:
                try:
                    intermed_dict[tuple] += 1
                except:
                    intermed_dict[tuple] = 1
            occurrences = sum([occ for occ in intermed_dict.values()])
            intermed_list = [[key, value] for key, value in intermed_dict.items()]
            intermed_list.append(occurrences)
            output_dictionnary[correct_analysis] = intermed_list
        output_list = [[key, value] for key, value in output_dictionnary.items()]
        output_list.sort(reverse=True, key=lambda x: x[-1][-1])

        readable = str()
        readable = "target prediction\n"
        for element, list_of_analysis in output_list:
            total_occurrence = list_of_analysis[-1]
            for analysis in list_of_analysis[:-1]:
                incorrect_analysis, occurrence = analysis
                to_string = [str(element) for element in incorrect_analysis]
                readable += f"{element} {' '.join(to_string)}: {str(occurrence)}\n"

        with open(f"{self.path_to_results}/{title}_quali.txt", "w") as quali:
            quali.write(readable)


    def quantitative_evaluation(self):
        """
        This function performs a global quantitative evaluation
        :return: a tsv file with lemma, pos, and morph error evaluation.
        The evaluation is performed text by text
        """

        results = {}
        for id, target in self.target_dict.items():
            lemma_errors = 0
            pos_errors = 0
            morph_errors = 0
            prediction = self.eval_dict[id]
            zipped_list = list(zip(target, prediction))
            aligned_analysis = [list(zip(targ, pred)) for targ, pred in zipped_list]
            aligned_analysis = [element for element in aligned_analysis if len(element) == 4]
            for element in aligned_analysis:
                try:
                    _, lemma, pos, morph = element
                except:
                    print(element)
                    print("Error")
                    exit(0)

                if len(set(lemma)) == 1:
                    # L'analyse est correcte
                    pass
                else:
                    lemma_errors += 1
                if len(set(pos)) == 1:
                    # L'analyse est correcte
                    pass
                else:
                    pos_errors += 1
                if len(set(morph)) == 1:
                    # L'analyse est correcte
                    pass
                else:
                    morph_errors += 1
            ratio_lemma = round(1 - (lemma_errors / len(aligned_analysis)), 3)
            ratio_pos = round(1 - (pos_errors / len(aligned_analysis)), 3)
            ratio_morph = round(1 - (morph_errors / len(aligned_analysis)), 3)
            results[id] = [str(len(aligned_analysis)), str(lemma_errors), str(pos_errors), str(morph_errors),
                           str(ratio_lemma), str(ratio_pos), f"{ratio_morph}\n"]

        with open(f"{self.path_to_results}/resultats_quanti.tsv", "w") as quanti_results:
            mis_en_forme = {id.replace("]", "").replace("[",""): '\t'.join(analysis) for id, analysis in results.items()}
            quanti_results.write(
                "ID\tNumber of tokens\tlemma errors\tpos errors\tmorph error\tlemma ratio\tpos ratio\tmorph ratio\n")
            for id, analysis in mis_en_forme.items():
                quanti_results.write(f"{id}\t{analysis}")
                
        with open(f"{self.path_to_results}/resultats_quanti.csv", "w") as quanti_results:
            mis_en_forme = {id.replace("]", "").replace("[",""): ','.join(analysis) for id, analysis in results.items()}
            quanti_results.write(
                "ID,Number of tokenvlemma errors,pos errors,morph error,lemma ratio,pos ratio,morph ratio\n")
            for id, analysis in mis_en_forme.items():
                quanti_results.write(f"{id},{analysis}")
                
    def create_reference_list_and_dicts(self):
        """
        This function creates the list (target, prediction) that is to be used by all
        other functions.
        :return: 
        """
        self.target_dict = self.to_dict_of_texts(self.target_list)
        self.eval_dict = self.to_dict_of_texts(self.list_to_evaluate)

        for id, target in self.target_dict.items():
            prediction = self.eval_dict[id]
            zipped_list = list(zip(target, prediction))
            for element in zipped_list:
                if element[0] == (['']):
                    continue
                else:
                    self.reference_aligned_list.append(element)


    def produce_global_list(self):
        """
        This function produces a list of list with the following entries:
        FORME CORRECT_LEMMA? CORRECT_POS? CORRECT_MORPH? TARGET_LEMMA TARGET_POS TARGET_MORPH PREDICTED_LEMMA PREDICTED_POS PREDICTED_MORPH
        :return:
        """
        output_list = []
        print(len(self.reference_aligned_list))
        for target, prediction in self.reference_aligned_list:
            interm_list = []
            target_form, target_lemma, target_pos, target_morph = target
            predict_form, predict_lemma, predict_pos, predict_morph = prediction
            interm_list.append(target_form)
            if target_lemma == predict_lemma:
                interm_list.append("True")
            else:
                interm_list.append("False")

            if target_pos == predict_pos:
                interm_list.append("True")
            else:
                interm_list.append("False")

            if target_morph == predict_morph:
                interm_list.append("True")
            else:
                interm_list.append("False")
            # https://stackoverflow.com/a/20196202
            interm_list.extend([target_lemma, target_pos, target_morph, predict_lemma, predict_pos, predict_morph])
            output_list.append("\t".join(interm_list))
        with open(f"{self.path_to_results}/exhaustive_list.tsv", "w") as exhaustive_list:
            exhaustive_list.write("FORM\tCORRECT_LEMMA?\tCORRECT_POS?\tCORRECT_MORPH?\tTARGET_LEMMA\tTARGET_POS\t"
                                  "TARGET_MORPH\tPREDICTED_LEMMA\tPREDICTED_POS\tPREDICTED_MORPH\n")
            exhaustive_list.write("\n".join(output_list))


    def qualitative_evaluation(self):
        """
        This function performs a qualitative evaluation, and produces twos different outputs:
        - and ordered list of incorrectly analyzed lemmas
        - and ordered list of incorrectly analyzed forms
        This evaluation is performed globally and also produces global metrics
        """


        self.target_dict = self.to_dict_of_texts(self.target_list)
        self.eval_dict = self.to_dict_of_texts(self.list_to_evaluate)

        try:
            len(self.list_to_evaluate) == len(self.target_list)
        except ValueError:
            print("The lists have different length. Check header or trailing linebreaks.")
        self.number_of_tokens = len(self.list_to_evaluate)
        nn_lemma = 0
        nn_morph = 0
        log = 0  # for debbuging purposes
        print(len(self.reference_aligned_list))
        for target_annotation, predict_annotation in self.reference_aligned_list:
            log += 1
            # we keep only the form, the lemma and the pos
            if len(target_annotation) == 1:
                continue

            try:
                form, lemma, pos, morph = predict_annotation
            except ValueError as error:
                print(f"Unpacking error on analysis {log}: {predict_annotation}. Please make sure "
                      f"the line contains: form, lemma, pos, morph, *, separed by a space")
                exit(0)

            if "[" in form and "]" in form:
                continue
            self.corpus_length += 1
            try:
                target_form, target_lemma, target_pos, target_morph = target_annotation
            except ValueError as e:
                print(target_annotation)
                print(e)
                print(f"Unpacking error for line {log} for file {self.target_file}: {print(target_annotation)}")
            if target_pos.startswith("NP"):
                self.number_of_named_entities += 1
            # let's compare each lemma with each other

            # Here we append the form and the lemma to two lists,
            # to create the vocabularies
            self.lemma_vocab.append(target_lemma)
            self.forms_vocab.append(target_form)
            if lemma == target_lemma:
                pass
            else:
                if self.ignore_ne_lemma and target_pos.startswith("NP"):
                    # we ignore named entities (add geographical entities?)
                    pass
                else:
                    nn_lemma += 1
                    self.lemma_errors += 1
                    self.global_errors += 1

                    # We also want to count global errors = total number of errors no matter if it affects pos or
                    # lemma. We have to subtract 1 if both lemma and pos are wrong (see below)
                    if pos != target_pos:
                        self.global_errors -= 1
                    try:
                        self.incorrect_lemma[target_lemma].append((form, lemma))
                        self.incorrect_forms[form].append((lemma, target_lemma, pos, morph))
                    except:
                        self.incorrect_lemma[target_lemma] = [(form, lemma)]
                        self.incorrect_forms[form] = [(lemma, target_lemma, pos, morph)]
                    try:
                        self.lemma_confusion_dict[form].append((lemma, target_lemma))
                    except KeyError:
                        self.lemma_confusion_dict[form] = [(lemma, target_lemma)]

            # we do the same for the pos
            if pos == target_pos:
                pass
            else:
                # vérifier qu'il faut pas soustraire le nombre total de NP dans ce cas.
                if self.ignore_ne_pos and target_pos.startswith("NP"):  # We ignore punctuation and named entities.
                    pass
                else:
                    self.pos_errors += 1
                    self.global_errors += 1
                    try:
                        self.pos_confusion_dict[target_pos].append((pos, morph, form, lemma))
                    except KeyError:
                        self.pos_confusion_dict[target_pos] = [(pos, morph, form, lemma)]

                    try:
                        self.incorrect_pos[target_pos] += 1
                    except:
                        self.incorrect_pos[target_pos] = 1


            # and for the morph
            if morph == target_morph:
                pass
            else:
                if self.ignore_ne_morph and target_pos.startswith("NP"):  # We ignore punctuation and named entities.
                    pass
                else:
                    nn_morph += 1
                    self.morph_errors += 1
                    self.global_errors += 1
                    try:
                        self.morph_confusion_dict[target_morph].append((pos, morph, form, lemma))
                    except KeyError:
                        self.morph_confusion_dict[target_morph] = [(pos, morph, form, lemma)]

                    try:
                        self.incorrect_morph[target_morph] += 1
                    except:
                        self.incorrect_morph[target_morph] = 1
        # On nettoie et on trie les dictionnaires
        # Lemmes

        output_dict = {}
        for key, value in self.incorrect_lemma.items():
            intermed_dict = {}
            inter_numb = 0
            for form, prediction in value:
                inter_numb += 1
                try:
                    intermed_dict[f'{form} > {prediction}'] += 1
                except:
                    intermed_dict[f'{form} > {prediction}'] = 1
            intermed_list = [f"{key}: {value}" for key, value in intermed_dict.items()]
            intermed_list.append(inter_numb)
            output_dict[key] = intermed_list

        for key, value in output_dict.items():
            self.lemma_errors_stats_list.append((f"{key.upper()}: {'; '.join(value[:-1])}\n", value[-1]))
        self.lemma_errors_stats_list.sort(key=lambda x: x[1], reverse=True)
        self.lemma_errors_stats_list = [f"{occ}\t{errors}" for errors, occ in self.lemma_errors_stats_list]


        # Now we create the vocabulary: number of different lemmas, and number of different forms
        self.lemma_vocab_length = len(list(set(self.lemma_vocab)))
        self.forms_vocab_length = len(list(set(self.forms_vocab)))
        print(self.corpus_length)

        self.print_confusion_dict(self.lemma_confusion_dict, "lemma")
        self.print_confusion_dict(self.morph_confusion_dict, "morph")
        self.print_confusion_dict(self.pos_confusion_dict, "pos")
        self.print_forms_list(self.incorrect_forms)


if __name__ == '__main__':
    # Params
    parser = argparse.ArgumentParser(description="Evaluate files in eval/ against ground truths in target/.")
    parser.add_argument("-s", "--separator", help="Separator for anotated file", default=" ")
    parser.add_argument("-c", "--path_to_corpus", help="Name of the path to the corpus. Both files must be named corpus.txt")
    args = parser.parse_args()
    separator = args.separator
    corpus = Corpus(path_to_corpus=args.path_to_corpus, ignore_ne_lemma=True, ignore_ne_pos=True, ignore_ne_morph=True, ignore_verb_type=False)
    corpus.split_pos_morph()
    corpus.create_reference_list_and_dicts()
    corpus.quantitative_evaluation()
    corpus.qualitative_evaluation()
    corpus.global_result()
    corpus.produce_global_list()
    corpus.request(r"IP[1-3][SP]0", "morph", "Present indicatif")
    corpus.request(r"IS[1-3][SP]0", "morph", "Parfait indicatif")
    corpus.request(r"SF[1-3][SP]0", "morph", "Indicatif futur")
    corpus.request(r"IF[1-3][SP]0", "morph", "Subjonctif futur")
    corpus.request(r"II[1-3][SP]0", "morph", "Indicatif imparfait")
    corpus.request(r"M.[1-3][SP]0", "morph", "Impératif")
    corpus.request(r"..[1-3][SP]0", "morph", "Verbes [morph]")
    corpus.request(r"[MFC][SP]000", "morph", "Noms propres [morph]")
    corpus.request(r"0[MFC][SP]00", "morph", "Adjectifs [morph]")
    corpus.request(r"V", "pos", "Verbes")
    corpus.request(r"A.", "pos", "adjectifs")
    corpus.request(r"S.", "pos", "prépositions")
    corpus.request(r"R.", "pos", "adverbes")
    corpus.request(r"N.", "pos", "Noms")
    corpus.request(r"NC", "pos", "Noms communs")
    corpus.request(r"NP", "pos", "Noms propres")
    corpus.request(r"000P0", "morph", "Noms propres personne")
    corpus.request(r"000G0", "morph", "Noms propres lieux")
    corpus.get_ambiguous_tokens_stats()