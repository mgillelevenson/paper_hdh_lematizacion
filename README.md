# Construcción de Un Corpus de Evaluación de La Anotación Léxico-Gramatical Del Castellano Medieval (Siglos 13-15)

Este depósito contiene el corpus de evaluación del castellano medieval presentado en el congreso HDH21, así como los programas que 
han permitido evaluar la herramienta Freeling. 


## Cómo funciona el programa de evaluación

Se comparan uno a uno cada uno de los documentos en los directorios `eval` (anotación producida por
el programa que queremos evaluar)
con los documentos correspondientes (mismo nombre) en el 
directorio `target` (anotación 'correcta', corregida a mano). 

`python3 evaluation_script.py -c path_to_corpus`




## Método de obtención de los datos 

Los datos provienen de más de 160 obras literarias de los siglos 13, 14 y 15, en ediciones lo más actuales posibles, 
escaneadas en su mayoría.  Luego se ha procedido a OCR, corrección del OCR y corrección de los análisis. 

`analyze -f /usr/local/share/freeling/config/es-old.cfg --nortk --nortkcon --input freeling --inplv splitted < $input > $output`


## Resultados

Los resultados están en el directorio `results/`.


## Autoría

Matthias Gille Levenson

Olivier Brisville-Fertin

María Díez Yáñez

Simon Gabay

## Licencia


Los materiales de este corpus, aun en construcción, se publican bajo licencia
<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">
<img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a> 
(Atribución-NoComercial-SinDerivadas 4.0 Internacional).




## Citar el corpus


```text
@inproceedings{GBDG_ConstruccionCorpusEvaluacion_2021,
  title = {Construcción de Un Corpus de Evaluación de La Anotación Léxico-Gramatical Del Castellano Medieval (Siglos 13-15)},
  author = {Gille Levenson, Matthias and Brisville-Fertin, Olivier and Díez Yáñez, Maria and Gabay, Simon},
  date = {2021},
  location = {{Santiago de Compostela}},
  eventtitle = {V Congreso de La {{Sociedad Internacional}} de {{Humanidades Digitales Hispánicas}}}
}
```
