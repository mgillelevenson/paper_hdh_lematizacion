import sys

def remove_accents(string):
    string = string.replace("á", "a")
    string = string.replace("é", "e")
    string = string.replace("í", "i")
    string = string.replace("ó", "o")
    string = string.replace("ú", "u")
    string = string.replace("ý", "y")
    string = string.replace("ï", "i")
    return string

def main(file):
    with open(file, "r") as input_file:
        input_list = [line.replace("\n", "") for line in input_file.readlines()]

    splitted_list = [line.split(" ") for line in input_list]

    output_list = []
    for line in splitted_list:
        if len(line) != 4:
            output_list.append("\n")
        else:
            form, lemma, pos, _ = line
            output_list.append(" ".join([remove_accents(form), lemma, pos, _, "\n"]))


    with open("no_accents.txt", "w") as output_file:
        output_file.write("".join(output_list))


if __name__ == '__main__':
    file = sys.argv[1]
    main(file)
